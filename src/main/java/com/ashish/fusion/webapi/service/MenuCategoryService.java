package com.ashish.fusion.webapi.service;

import com.ashish.fusion.webapi.domain.MenuCategory;
import com.ashish.fusion.webapi.repository.MenuCategoryRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing MenuCategory.
 */
@Service
@Transactional
public class MenuCategoryService {

    private final Logger log = LoggerFactory.getLogger(MenuCategoryService.class);

    private final MenuCategoryRepository menuCategoryRepository;

    public MenuCategoryService(MenuCategoryRepository menuCategoryRepository) {
        this.menuCategoryRepository = menuCategoryRepository;
    }

    /**
     * Save a menuCategory.
     *
     * @param menuCategory the entity to save
     * @return the persisted entity
     */
    public MenuCategory save(MenuCategory menuCategory) {
        log.debug("Request to save MenuCategory : {}", menuCategory);
        return menuCategoryRepository.save(menuCategory);
    }

    /**
     * Get all the menuCategories.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<MenuCategory> findAll() {
        log.debug("Request to get all MenuCategories");
        return menuCategoryRepository.findAll();
    }


    /**
     * Get one menuCategory by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<MenuCategory> findOne(Long id) {
        log.debug("Request to get MenuCategory : {}", id);
        return menuCategoryRepository.findById(id);
    }

    /**
     * Delete the menuCategory by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete MenuCategory : {}", id);
        menuCategoryRepository.deleteById(id);
    }
}
