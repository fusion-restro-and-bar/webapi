package com.ashish.fusion.webapi.service;

import com.ashish.fusion.webapi.domain.RestroTables;
import com.ashish.fusion.webapi.repository.RestroTablesRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing RestroTables.
 */
@Service
@Transactional
public class RestroTablesService {

    private final Logger log = LoggerFactory.getLogger(RestroTablesService.class);

    private final RestroTablesRepository restroTablesRepository;

    public RestroTablesService(RestroTablesRepository restroTablesRepository) {
        this.restroTablesRepository = restroTablesRepository;
    }

    /**
     * Save a restroTables.
     *
     * @param restroTables the entity to save
     * @return the persisted entity
     */
    public RestroTables save(RestroTables restroTables) {
        log.debug("Request to save RestroTables : {}", restroTables);
        return restroTablesRepository.save(restroTables);
    }

    /**
     * Get all the restroTables.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<RestroTables> findAll() {
        log.debug("Request to get all RestroTables");
        return restroTablesRepository.findAll();
    }


    /**
     * Get one restroTables by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<RestroTables> findOne(Long id) {
        log.debug("Request to get RestroTables : {}", id);
        return restroTablesRepository.findById(id);
    }

    /**
     * Delete the restroTables by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete RestroTables : {}", id);
        restroTablesRepository.deleteById(id);
    }
}
