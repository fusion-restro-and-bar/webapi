package com.ashish.fusion.webapi.service;

import com.ashish.fusion.webapi.domain.BookingOrderHistory;
import com.ashish.fusion.webapi.repository.BookingOrderHistoryRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing BookingOrderHistory.
 */
@Service
@Transactional
public class BookingOrderHistoryService {

    private final Logger log = LoggerFactory.getLogger(BookingOrderHistoryService.class);

    private final BookingOrderHistoryRepository bookingOrderHistoryRepository;

    public BookingOrderHistoryService(BookingOrderHistoryRepository bookingOrderHistoryRepository) {
        this.bookingOrderHistoryRepository = bookingOrderHistoryRepository;
    }

    /**
     * Save a bookingOrderHistory.
     *
     * @param bookingOrderHistory the entity to save
     * @return the persisted entity
     */
    public BookingOrderHistory save(BookingOrderHistory bookingOrderHistory) {
        log.debug("Request to save BookingOrderHistory : {}", bookingOrderHistory);
        return bookingOrderHistoryRepository.save(bookingOrderHistory);
    }

    /**
     * Get all the bookingOrderHistories.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<BookingOrderHistory> findAll() {
        log.debug("Request to get all BookingOrderHistories");
        return bookingOrderHistoryRepository.findAllWithEagerRelationships();
    }

    /**
     * Get all the BookingOrderHistory with eager load of many-to-many relationships.
     *
     * @return the list of entities
     */
    public Page<BookingOrderHistory> findAllWithEagerRelationships(Pageable pageable) {
        return bookingOrderHistoryRepository.findAllWithEagerRelationships(pageable);
    }
    

    /**
     * Get one bookingOrderHistory by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<BookingOrderHistory> findOne(Long id) {
        log.debug("Request to get BookingOrderHistory : {}", id);
        return bookingOrderHistoryRepository.findOneWithEagerRelationships(id);
    }

    /**
     * Delete the bookingOrderHistory by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete BookingOrderHistory : {}", id);
        bookingOrderHistoryRepository.deleteById(id);
    }
}
