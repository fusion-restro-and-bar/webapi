package com.ashish.fusion.webapi.service;

import com.ashish.fusion.webapi.domain.MenuItem;
import com.ashish.fusion.webapi.repository.MenuItemRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing MenuItem.
 */
@Service
@Transactional
public class MenuItemService {

    private final Logger log = LoggerFactory.getLogger(MenuItemService.class);

    private final MenuItemRepository menuItemRepository;

    public MenuItemService(MenuItemRepository menuItemRepository) {
        this.menuItemRepository = menuItemRepository;
    }

    /**
     * Save a menuItem.
     *
     * @param menuItem the entity to save
     * @return the persisted entity
     */
    public MenuItem save(MenuItem menuItem) {
        log.debug("Request to save MenuItem : {}", menuItem);
        return menuItemRepository.save(menuItem);
    }

    /**
     * Get all the menuItems.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<MenuItem> findAll() {
        log.debug("Request to get all MenuItems");
        return menuItemRepository.findAll();
    }


    /**
     * Get one menuItem by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<MenuItem> findOne(Long id) {
        log.debug("Request to get MenuItem : {}", id);
        return menuItemRepository.findById(id);
    }

    /**
     * Delete the menuItem by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete MenuItem : {}", id);
        menuItemRepository.deleteMenuItemById(id);
    }
}
