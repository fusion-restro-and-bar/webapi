package com.ashish.fusion.webapi.service;

import com.ashish.fusion.webapi.domain.BookingOrder;
import com.ashish.fusion.webapi.repository.BookingOrderRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing BookingOrder.
 */
@Service
@Transactional
public class BookingOrderService {

    private final Logger log = LoggerFactory.getLogger(BookingOrderService.class);

    private final BookingOrderRepository bookingOrderRepository;

    public BookingOrderService(BookingOrderRepository bookingOrderRepository) {
        this.bookingOrderRepository = bookingOrderRepository;
    }

    /**
     * Save a bookingOrder.
     *
     * @param bookingOrder the entity to save
     * @return the persisted entity
     */
    public BookingOrder save(BookingOrder bookingOrder) {
        log.debug("Request to save BookingOrder : {}", bookingOrder);
        return bookingOrderRepository.save(bookingOrder);
    }

    /**
     * Get all the bookingOrders.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<BookingOrder> findAll() {
        log.debug("Request to get all BookingOrders");
        return bookingOrderRepository.findAllWithEagerRelationships();
    }

    /**
     * Get all the BookingOrder with eager load of many-to-many relationships.
     *
     * @return the list of entities
     */
    public Page<BookingOrder> findAllWithEagerRelationships(Pageable pageable) {
        return bookingOrderRepository.findAllWithEagerRelationships(pageable);
    }
    

    /**
     * Get one bookingOrder by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<BookingOrder> findOne(Long id) {
        log.debug("Request to get BookingOrder : {}", id);
        return bookingOrderRepository.findOneWithEagerRelationships(id);
    }

    /**
     * Delete the bookingOrder by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete BookingOrder : {}", id);
        bookingOrderRepository.deleteById(id);
    }
}
