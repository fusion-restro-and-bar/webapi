package com.ashish.fusion.webapi.repository;

import com.ashish.fusion.webapi.domain.RestroTables;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the RestroTables entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RestroTablesRepository extends JpaRepository<RestroTables, Long> {

}
