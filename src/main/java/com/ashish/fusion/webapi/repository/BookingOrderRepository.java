package com.ashish.fusion.webapi.repository;

import com.ashish.fusion.webapi.domain.BookingOrder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the BookingOrder entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BookingOrderRepository extends JpaRepository<BookingOrder, Long> {

    @Query(value = "select distinct booking_order from BookingOrder booking_order left join fetch booking_order.restroTables left join fetch booking_order.menuItems",
        countQuery = "select count(distinct booking_order) from BookingOrder booking_order")
    Page<BookingOrder> findAllWithEagerRelationships(Pageable pageable);

    @Query(value = "select distinct booking_order from BookingOrder booking_order left join fetch booking_order.restroTables left join fetch booking_order.menuItems")
    List<BookingOrder> findAllWithEagerRelationships();

    @Query("select booking_order from BookingOrder booking_order left join fetch booking_order.restroTables left join fetch booking_order.menuItems where booking_order.id =:id")
    Optional<BookingOrder> findOneWithEagerRelationships(@Param("id") Long id);

}
