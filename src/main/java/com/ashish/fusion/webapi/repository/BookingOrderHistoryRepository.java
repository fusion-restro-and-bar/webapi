package com.ashish.fusion.webapi.repository;

import com.ashish.fusion.webapi.domain.BookingOrderHistory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the BookingOrderHistory entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BookingOrderHistoryRepository extends JpaRepository<BookingOrderHistory, Long> {

    @Query(value = "select distinct booking_order_history from BookingOrderHistory booking_order_history left join fetch booking_order_history.restroTables left join fetch booking_order_history.menuItems",
        countQuery = "select count(distinct booking_order_history) from BookingOrderHistory booking_order_history")
    Page<BookingOrderHistory> findAllWithEagerRelationships(Pageable pageable);

    @Query(value = "select distinct booking_order_history from BookingOrderHistory booking_order_history left join fetch booking_order_history.restroTables left join fetch booking_order_history.menuItems")
    List<BookingOrderHistory> findAllWithEagerRelationships();

    @Query("select booking_order_history from BookingOrderHistory booking_order_history left join fetch booking_order_history.restroTables left join fetch booking_order_history.menuItems where booking_order_history.id =:id")
    Optional<BookingOrderHistory> findOneWithEagerRelationships(@Param("id") Long id);

}
