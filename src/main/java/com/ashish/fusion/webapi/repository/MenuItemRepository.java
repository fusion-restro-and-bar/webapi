package com.ashish.fusion.webapi.repository;

import com.ashish.fusion.webapi.domain.MenuItem;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the MenuItem entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MenuItemRepository extends JpaRepository<MenuItem, Long> {

    @Query("DELETE FROM MenuItem mi where mi.id = ?1")
    @Modifying
    void deleteMenuItemById(Long id);
}
