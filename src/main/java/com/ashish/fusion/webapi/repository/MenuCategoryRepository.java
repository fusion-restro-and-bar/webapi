package com.ashish.fusion.webapi.repository;

import com.ashish.fusion.webapi.domain.MenuCategory;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the MenuCategory entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MenuCategoryRepository extends JpaRepository<MenuCategory, Long> {

}
