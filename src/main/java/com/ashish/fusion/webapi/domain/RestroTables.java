package com.ashish.fusion.webapi.domain;


import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A RestroTables.
 */
@Entity
@Table(name = "restro_tables")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class RestroTables implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "table_name")
    private String tableName;

    @Column(name = "status")
    private String status;

    @Column(name = "is_deleted")
    private Boolean isDeleted;

    @ManyToMany(mappedBy = "restroTables")
    private Set<BookingOrder> bookingOrders = new HashSet<>();

    @ManyToMany(mappedBy = "restroTables")
    private Set<BookingOrderHistory> bookingOrderHistories = new HashSet<>();

    public Set<BookingOrderHistory> getBookingOrderHistories() {
        return bookingOrderHistories;
    }

    public void setBookingOrderHistories(Set<BookingOrderHistory> bookingOrderHistories) {
        this.bookingOrderHistories = bookingOrderHistories;
    }

    public Boolean getDeleted() {
        return isDeleted;
    }

    public void setDeleted(Boolean deleted) {
        isDeleted = deleted;
    }

    public Set<BookingOrder> getBookingOrders() {
        return bookingOrders;
    }

    public void setBookingOrders(Set<BookingOrder> bookingOrders) {
        this.bookingOrders = bookingOrders;
    }

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTableName() {
        return tableName;
    }

    public RestroTables tableName(String tableName) {
        this.tableName = tableName;
        return this;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getStatus() {
        return status;
    }

    public RestroTables status(String status) {
        this.status = status;
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Boolean isIsDeleted() {
        return isDeleted;
    }

    public RestroTables isDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
        return this;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RestroTables restroTables = (RestroTables) o;
        if (restroTables.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), restroTables.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "RestroTables{" +
            "id=" + getId() +
            ", tableName='" + getTableName() + "'" +
            ", status='" + getStatus() + "'" +
            ", isDeleted='" + isIsDeleted() + "'" +
            "}";
    }
}
