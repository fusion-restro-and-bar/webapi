package com.ashish.fusion.webapi.domain;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A MenuItem.
 */
@Entity
@Table(name = "menu_item")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class MenuItem implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "item_name")
    private String itemName;

    @Column(name = "price")
    private Float price;

    @ManyToOne()
    @JsonBackReference
    private MenuCategory menuCategory;

    @ManyToMany(mappedBy = "menuItems")
    private Set<BookingOrder> bookingOrders = new HashSet<>();

    @ManyToMany(mappedBy = "menuItems")
    private Set<BookingOrderHistory> bookingOrderHistories = new HashSet<>();

    public Set<BookingOrder> getBookingOrders() {
        return bookingOrders;
    }

    public void setBookingOrders(Set<BookingOrder> bookingOrders) {
        this.bookingOrders = bookingOrders;
    }

    public Set<BookingOrderHistory> getBookingOrderHistories() {
        return bookingOrderHistories;
    }

    public void setBookingOrderHistories(Set<BookingOrderHistory> bookingOrderHistories) {
        this.bookingOrderHistories = bookingOrderHistories;
    }

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getItemName() {
        return itemName;
    }

    public MenuItem itemName(String itemName) {
        this.itemName = itemName;
        return this;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public Float getPrice() {
        return price;
    }

    public MenuItem price(Float price) {
        this.price = price;
        return this;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public MenuCategory getMenuCategory() {
        return menuCategory;
    }

    public MenuItem menuCategory(MenuCategory menuCategory) {
        this.menuCategory = menuCategory;
        return this;
    }

    public void setMenuCategory(MenuCategory menuCategory) {
        this.menuCategory = menuCategory;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MenuItem menuItem = (MenuItem) o;
        if (menuItem.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), menuItem.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "MenuItem{" +
            "id=" + getId() +
            ", itemName='" + getItemName() + "'" +
            ", price=" + getPrice() +
            "}";
    }
}
