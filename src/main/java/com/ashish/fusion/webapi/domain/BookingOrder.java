package com.ashish.fusion.webapi.domain;


import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A BookingOrder.
 */
@Entity
@Table(name = "booking_order")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class BookingOrder implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "order_date")
    private LocalDate orderDate;

    @Column(name = "total_bill")
    private Float totalBill;

    @Column(name = "status")
    private String status;

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "booking_order_restro_tables",
               joinColumns = @JoinColumn(name = "booking_order_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "restro_tables_id", referencedColumnName = "id"))
    private Set<RestroTables> restroTables = new HashSet<>();

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "booking_order_menu_item",
               joinColumns = @JoinColumn(name = "booking_order_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "menu_item_id", referencedColumnName = "id"))
    private Set<MenuItem> menuItems = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getOrderDate() {
        return orderDate;
    }

    public BookingOrder orderDate(LocalDate orderDate) {
        this.orderDate = orderDate;
        return this;
    }

    public void setOrderDate(LocalDate orderDate) {
        this.orderDate = orderDate;
    }

    public Float getTotalBill() {
        return totalBill;
    }

    public BookingOrder totalBill(Float totalBill) {
        this.totalBill = totalBill;
        return this;
    }

    public void setTotalBill(Float totalBill) {
        this.totalBill = totalBill;
    }

    public String getStatus() {
        return status;
    }

    public BookingOrder status(String status) {
        this.status = status;
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Set<RestroTables> getRestroTables() {
        return restroTables;
    }

    public BookingOrder restroTables(Set<RestroTables> restroTables) {
        this.restroTables = restroTables;
        return this;
    }

    public BookingOrder addRestroTables(RestroTables restroTables) {
        this.restroTables.add(restroTables);
        restroTables.getBookingOrders().add(this);
        return this;
    }

    public BookingOrder removeRestroTables(RestroTables restroTables) {
        this.restroTables.remove(restroTables);
        restroTables.getBookingOrders().remove(this);
        return this;
    }

    public void setRestroTables(Set<RestroTables> restroTables) {
        this.restroTables = restroTables;
    }

    public Set<MenuItem> getMenuItems() {
        return menuItems;
    }

    public BookingOrder menuItems(Set<MenuItem> menuItems) {
        this.menuItems = menuItems;
        return this;
    }

    public BookingOrder addMenuItem(MenuItem menuItem) {
        this.menuItems.add(menuItem);
        menuItem.getBookingOrders().add(this);
        return this;
    }

    public BookingOrder removeMenuItem(MenuItem menuItem) {
        this.menuItems.remove(menuItem);
        menuItem.getBookingOrders().remove(this);
        return this;
    }

    public void setMenuItems(Set<MenuItem> menuItems) {
        this.menuItems = menuItems;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        BookingOrder bookingOrder = (BookingOrder) o;
        if (bookingOrder.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), bookingOrder.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "BookingOrder{" +
            "id=" + getId() +
            ", orderDate='" + getOrderDate() + "'" +
            ", totalBill=" + getTotalBill() +
            ", status='" + getStatus() + "'" +
            "}";
    }
}
