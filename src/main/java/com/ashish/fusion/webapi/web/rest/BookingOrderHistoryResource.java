package com.ashish.fusion.webapi.web.rest;
import com.ashish.fusion.webapi.domain.BookingOrderHistory;
import com.ashish.fusion.webapi.service.BookingOrderHistoryService;
import com.ashish.fusion.webapi.web.rest.errors.BadRequestAlertException;
import com.ashish.fusion.webapi.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing BookingOrderHistory.
 */
@RestController
@RequestMapping("/api")
public class BookingOrderHistoryResource {

    private final Logger log = LoggerFactory.getLogger(BookingOrderHistoryResource.class);

    private static final String ENTITY_NAME = "bookingOrderHistory";

    private final BookingOrderHistoryService bookingOrderHistoryService;

    public BookingOrderHistoryResource(BookingOrderHistoryService bookingOrderHistoryService) {
        this.bookingOrderHistoryService = bookingOrderHistoryService;
    }

    /**
     * POST  /booking-order-histories : Create a new bookingOrderHistory.
     *
     * @param bookingOrderHistory the bookingOrderHistory to create
     * @return the ResponseEntity with status 201 (Created) and with body the new bookingOrderHistory, or with status 400 (Bad Request) if the bookingOrderHistory has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/booking-order-histories")
    public ResponseEntity<BookingOrderHistory> createBookingOrderHistory(@RequestBody BookingOrderHistory bookingOrderHistory) throws URISyntaxException {
        log.debug("REST request to save BookingOrderHistory : {}", bookingOrderHistory);
        if (bookingOrderHistory.getId() != null) {
            throw new BadRequestAlertException("A new bookingOrderHistory cannot already have an ID", ENTITY_NAME, "idexists");
        }
        BookingOrderHistory result = bookingOrderHistoryService.save(bookingOrderHistory);
        return ResponseEntity.created(new URI("/api/booking-order-histories/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /booking-order-histories : Updates an existing bookingOrderHistory.
     *
     * @param bookingOrderHistory the bookingOrderHistory to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated bookingOrderHistory,
     * or with status 400 (Bad Request) if the bookingOrderHistory is not valid,
     * or with status 500 (Internal Server Error) if the bookingOrderHistory couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/booking-order-histories")
    public ResponseEntity<BookingOrderHistory> updateBookingOrderHistory(@RequestBody BookingOrderHistory bookingOrderHistory) throws URISyntaxException {
        log.debug("REST request to update BookingOrderHistory : {}", bookingOrderHistory);
        if (bookingOrderHistory.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        BookingOrderHistory result = bookingOrderHistoryService.save(bookingOrderHistory);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, bookingOrderHistory.getId().toString()))
            .body(result);
    }

    /**
     * GET  /booking-order-histories : get all the bookingOrderHistories.
     *
     * @param eagerload flag to eager load entities from relationships (This is applicable for many-to-many)
     * @return the ResponseEntity with status 200 (OK) and the list of bookingOrderHistories in body
     */
    @GetMapping("/booking-order-histories")
    public List<BookingOrderHistory> getAllBookingOrderHistories(@RequestParam(required = false, defaultValue = "false") boolean eagerload) {
        log.debug("REST request to get all BookingOrderHistories");
        return bookingOrderHistoryService.findAll();
    }

    /**
     * GET  /booking-order-histories/:id : get the "id" bookingOrderHistory.
     *
     * @param id the id of the bookingOrderHistory to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the bookingOrderHistory, or with status 404 (Not Found)
     */
    @GetMapping("/booking-order-histories/{id}")
    public ResponseEntity<BookingOrderHistory> getBookingOrderHistory(@PathVariable Long id) {
        log.debug("REST request to get BookingOrderHistory : {}", id);
        Optional<BookingOrderHistory> bookingOrderHistory = bookingOrderHistoryService.findOne(id);
        return ResponseUtil.wrapOrNotFound(bookingOrderHistory);
    }

    /**
     * DELETE  /booking-order-histories/:id : delete the "id" bookingOrderHistory.
     *
     * @param id the id of the bookingOrderHistory to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/booking-order-histories/{id}")
    public ResponseEntity<Void> deleteBookingOrderHistory(@PathVariable Long id) {
        log.debug("REST request to delete BookingOrderHistory : {}", id);
        bookingOrderHistoryService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
