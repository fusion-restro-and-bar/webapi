package com.ashish.fusion.webapi.web.rest;
import com.ashish.fusion.webapi.domain.RestroTables;
import com.ashish.fusion.webapi.service.RestroTablesService;
import com.ashish.fusion.webapi.web.rest.errors.BadRequestAlertException;
import com.ashish.fusion.webapi.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing RestroTables.
 */
@RestController
@RequestMapping("/api")
public class RestroTablesResource {

    private final Logger log = LoggerFactory.getLogger(RestroTablesResource.class);

    private static final String ENTITY_NAME = "restroTables";

    private final RestroTablesService restroTablesService;

    public RestroTablesResource(RestroTablesService restroTablesService) {
        this.restroTablesService = restroTablesService;
    }

    /**
     * POST  /restro-tables : Create a new restroTables.
     *
     * @param restroTables the restroTables to create
     * @return the ResponseEntity with status 201 (Created) and with body the new restroTables, or with status 400 (Bad Request) if the restroTables has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/restro-tables")
    public ResponseEntity<RestroTables> createRestroTables(@RequestBody RestroTables restroTables) throws URISyntaxException {
        log.debug("REST request to save RestroTables : {}", restroTables);
        if (restroTables.getId() != null) {
            throw new BadRequestAlertException("A new restroTables cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RestroTables result = restroTablesService.save(restroTables);
        return ResponseEntity.created(new URI("/api/restro-tables/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /restro-tables : Updates an existing restroTables.
     *
     * @param restroTables the restroTables to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated restroTables,
     * or with status 400 (Bad Request) if the restroTables is not valid,
     * or with status 500 (Internal Server Error) if the restroTables couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/restro-tables")
    public ResponseEntity<RestroTables> updateRestroTables(@RequestBody RestroTables restroTables) throws URISyntaxException {
        log.debug("REST request to update RestroTables : {}", restroTables);
        if (restroTables.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        RestroTables result = restroTablesService.save(restroTables);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, restroTables.getId().toString()))
            .body(result);
    }

    /**
     * GET  /restro-tables : get all the restroTables.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of restroTables in body
     */
    @GetMapping("/restro-tables")
    public List<RestroTables> getAllRestroTables() {
        log.debug("REST request to get all RestroTables");
        return restroTablesService.findAll();
    }

    /**
     * GET  /restro-tables/:id : get the "id" restroTables.
     *
     * @param id the id of the restroTables to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the restroTables, or with status 404 (Not Found)
     */
    @GetMapping("/restro-tables/{id}")
    public ResponseEntity<RestroTables> getRestroTables(@PathVariable Long id) {
        log.debug("REST request to get RestroTables : {}", id);
        Optional<RestroTables> restroTables = restroTablesService.findOne(id);
        return ResponseUtil.wrapOrNotFound(restroTables);
    }

    /**
     * DELETE  /restro-tables/:id : delete the "id" restroTables.
     *
     * @param id the id of the restroTables to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/restro-tables/{id}")
    public ResponseEntity<Void> deleteRestroTables(@PathVariable Long id) {
        log.debug("REST request to delete RestroTables : {}", id);
        restroTablesService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
