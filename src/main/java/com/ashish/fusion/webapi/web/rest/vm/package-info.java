/**
 * View Models used by Spring MVC REST controllers.
 */
package com.ashish.fusion.webapi.web.rest.vm;
