package com.ashish.fusion.webapi.web.rest;
import com.ashish.fusion.webapi.domain.MenuCategory;
import com.ashish.fusion.webapi.service.MenuCategoryService;
import com.ashish.fusion.webapi.web.rest.errors.BadRequestAlertException;
import com.ashish.fusion.webapi.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing MenuCategory.
 */
@RestController
@RequestMapping("/api")
public class MenuCategoryResource {

    private final Logger log = LoggerFactory.getLogger(MenuCategoryResource.class);

    private static final String ENTITY_NAME = "menuCategory";

    private final MenuCategoryService menuCategoryService;

    public MenuCategoryResource(MenuCategoryService menuCategoryService) {
        this.menuCategoryService = menuCategoryService;
    }

    /**
     * POST  /menu-categories : Create a new menuCategory.
     *
     * @param menuCategory the menuCategory to create
     * @return the ResponseEntity with status 201 (Created) and with body the new menuCategory, or with status 400 (Bad Request) if the menuCategory has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/menu-categories")
    public ResponseEntity<MenuCategory> createMenuCategory(@RequestBody MenuCategory menuCategory) throws URISyntaxException {
        log.debug("REST request to save MenuCategory : {}", menuCategory);
        if (menuCategory.getId() != null) {
            throw new BadRequestAlertException("A new menuCategory cannot already have an ID", ENTITY_NAME, "idexists");
        }
        MenuCategory result = menuCategoryService.save(menuCategory);
        return ResponseEntity.created(new URI("/api/menu-categories/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /menu-categories : Updates an existing menuCategory.
     *
     * @param menuCategory the menuCategory to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated menuCategory,
     * or with status 400 (Bad Request) if the menuCategory is not valid,
     * or with status 500 (Internal Server Error) if the menuCategory couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/menu-categories")
    public ResponseEntity<MenuCategory> updateMenuCategory(@RequestBody MenuCategory menuCategory) throws URISyntaxException {
        log.debug("REST request to update MenuCategory : {}", menuCategory);
        if (menuCategory.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        MenuCategory result = menuCategoryService.save(menuCategory);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, menuCategory.getId().toString()))
            .body(result);
    }

    /**
     * GET  /menu-categories : get all the menuCategories.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of menuCategories in body
     */
    @GetMapping("/menu-categories")
    public List<MenuCategory> getAllMenuCategories() {
        log.debug("REST request to get all MenuCategories");
        return menuCategoryService.findAll();
    }

    /**
     * GET  /menu-categories/:id : get the "id" menuCategory.
     *
     * @param id the id of the menuCategory to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the menuCategory, or with status 404 (Not Found)
     */
    @GetMapping("/menu-categories/{id}")
    public ResponseEntity<MenuCategory> getMenuCategory(@PathVariable Long id) {
        log.debug("REST request to get MenuCategory : {}", id);
        Optional<MenuCategory> menuCategory = menuCategoryService.findOne(id);
        return ResponseUtil.wrapOrNotFound(menuCategory);
    }

    /**
     * DELETE  /menu-categories/:id : delete the "id" menuCategory.
     *
     * @param id the id of the menuCategory to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/menu-categories/{id}")
    public ResponseEntity<Void> deleteMenuCategory(@PathVariable Long id) {
        log.debug("REST request to delete MenuCategory : {}", id);
        menuCategoryService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
