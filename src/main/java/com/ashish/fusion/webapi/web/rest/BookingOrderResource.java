package com.ashish.fusion.webapi.web.rest;
import com.ashish.fusion.webapi.domain.BookingOrder;
import com.ashish.fusion.webapi.service.BookingOrderService;
import com.ashish.fusion.webapi.web.rest.errors.BadRequestAlertException;
import com.ashish.fusion.webapi.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing BookingOrder.
 */
@RestController
@RequestMapping("/api")
public class BookingOrderResource {

    private final Logger log = LoggerFactory.getLogger(BookingOrderResource.class);

    private static final String ENTITY_NAME = "bookingOrder";

    private final BookingOrderService bookingOrderService;

    public BookingOrderResource(BookingOrderService bookingOrderService) {
        this.bookingOrderService = bookingOrderService;
    }

    /**
     * POST  /booking-orders : Create a new bookingOrder.
     *
     * @param bookingOrder the bookingOrder to create
     * @return the ResponseEntity with status 201 (Created) and with body the new bookingOrder, or with status 400 (Bad Request) if the bookingOrder has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/booking-orders")
    public ResponseEntity<BookingOrder> createBookingOrder(@RequestBody BookingOrder bookingOrder) throws URISyntaxException {
        log.debug("REST request to save BookingOrder : {}", bookingOrder);
        if (bookingOrder.getId() != null) {
            throw new BadRequestAlertException("A new bookingOrder cannot already have an ID", ENTITY_NAME, "idexists");
        }
        BookingOrder result = bookingOrderService.save(bookingOrder);
        return ResponseEntity.created(new URI("/api/booking-orders/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /booking-orders : Updates an existing bookingOrder.
     *
     * @param bookingOrder the bookingOrder to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated bookingOrder,
     * or with status 400 (Bad Request) if the bookingOrder is not valid,
     * or with status 500 (Internal Server Error) if the bookingOrder couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/booking-orders")
    public ResponseEntity<BookingOrder> updateBookingOrder(@RequestBody BookingOrder bookingOrder) throws URISyntaxException {
        log.debug("REST request to update BookingOrder : {}", bookingOrder);
        if (bookingOrder.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        BookingOrder result = bookingOrderService.save(bookingOrder);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, bookingOrder.getId().toString()))
            .body(result);
    }

    /**
     * GET  /booking-orders : get all the bookingOrders.
     *
     * @param eagerload flag to eager load entities from relationships (This is applicable for many-to-many)
     * @return the ResponseEntity with status 200 (OK) and the list of bookingOrders in body
     */
    @GetMapping("/booking-orders")
    public List<BookingOrder> getAllBookingOrders(@RequestParam(required = false, defaultValue = "false") boolean eagerload) {
        log.debug("REST request to get all BookingOrders");
        return bookingOrderService.findAll();
    }

    /**
     * GET  /booking-orders/:id : get the "id" bookingOrder.
     *
     * @param id the id of the bookingOrder to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the bookingOrder, or with status 404 (Not Found)
     */
    @GetMapping("/booking-orders/{id}")
    public ResponseEntity<BookingOrder> getBookingOrder(@PathVariable Long id) {
        log.debug("REST request to get BookingOrder : {}", id);
        Optional<BookingOrder> bookingOrder = bookingOrderService.findOne(id);
        return ResponseUtil.wrapOrNotFound(bookingOrder);
    }

    /**
     * DELETE  /booking-orders/:id : delete the "id" bookingOrder.
     *
     * @param id the id of the bookingOrder to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/booking-orders/{id}")
    public ResponseEntity<Void> deleteBookingOrder(@PathVariable Long id) {
        log.debug("REST request to delete BookingOrder : {}", id);
        bookingOrderService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
