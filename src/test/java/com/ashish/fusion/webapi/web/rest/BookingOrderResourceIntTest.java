package com.ashish.fusion.webapi.web.rest;

import com.ashish.fusion.webapi.WebapiApp;

import com.ashish.fusion.webapi.domain.BookingOrder;
import com.ashish.fusion.webapi.repository.BookingOrderRepository;
import com.ashish.fusion.webapi.service.BookingOrderService;
import com.ashish.fusion.webapi.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;


import static com.ashish.fusion.webapi.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the BookingOrderResource REST controller.
 *
 * @see BookingOrderResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WebapiApp.class)
public class BookingOrderResourceIntTest {

    private static final LocalDate DEFAULT_ORDER_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_ORDER_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final Float DEFAULT_TOTAL_BILL = 1F;
    private static final Float UPDATED_TOTAL_BILL = 2F;

    private static final String DEFAULT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_STATUS = "BBBBBBBBBB";

    @Autowired
    private BookingOrderRepository bookingOrderRepository;

    @Mock
    private BookingOrderRepository bookingOrderRepositoryMock;

    @Mock
    private BookingOrderService bookingOrderServiceMock;

    @Autowired
    private BookingOrderService bookingOrderService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restBookingOrderMockMvc;

    private BookingOrder bookingOrder;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final BookingOrderResource bookingOrderResource = new BookingOrderResource(bookingOrderService);
        this.restBookingOrderMockMvc = MockMvcBuilders.standaloneSetup(bookingOrderResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BookingOrder createEntity(EntityManager em) {
        BookingOrder bookingOrder = new BookingOrder()
            .orderDate(DEFAULT_ORDER_DATE)
            .totalBill(DEFAULT_TOTAL_BILL)
            .status(DEFAULT_STATUS);
        return bookingOrder;
    }

    @Before
    public void initTest() {
        bookingOrder = createEntity(em);
    }

    @Test
    @Transactional
    public void createBookingOrder() throws Exception {
        int databaseSizeBeforeCreate = bookingOrderRepository.findAll().size();

        // Create the BookingOrder
        restBookingOrderMockMvc.perform(post("/api/booking-orders")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bookingOrder)))
            .andExpect(status().isCreated());

        // Validate the BookingOrder in the database
        List<BookingOrder> bookingOrderList = bookingOrderRepository.findAll();
        assertThat(bookingOrderList).hasSize(databaseSizeBeforeCreate + 1);
        BookingOrder testBookingOrder = bookingOrderList.get(bookingOrderList.size() - 1);
        assertThat(testBookingOrder.getOrderDate()).isEqualTo(DEFAULT_ORDER_DATE);
        assertThat(testBookingOrder.getTotalBill()).isEqualTo(DEFAULT_TOTAL_BILL);
        assertThat(testBookingOrder.getStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    public void createBookingOrderWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = bookingOrderRepository.findAll().size();

        // Create the BookingOrder with an existing ID
        bookingOrder.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restBookingOrderMockMvc.perform(post("/api/booking-orders")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bookingOrder)))
            .andExpect(status().isBadRequest());

        // Validate the BookingOrder in the database
        List<BookingOrder> bookingOrderList = bookingOrderRepository.findAll();
        assertThat(bookingOrderList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllBookingOrders() throws Exception {
        // Initialize the database
        bookingOrderRepository.saveAndFlush(bookingOrder);

        // Get all the bookingOrderList
        restBookingOrderMockMvc.perform(get("/api/booking-orders?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(bookingOrder.getId().intValue())))
            .andExpect(jsonPath("$.[*].orderDate").value(hasItem(DEFAULT_ORDER_DATE.toString())))
            .andExpect(jsonPath("$.[*].totalBill").value(hasItem(DEFAULT_TOTAL_BILL.doubleValue())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())));
    }
    
    @SuppressWarnings({"unchecked"})
    public void getAllBookingOrdersWithEagerRelationshipsIsEnabled() throws Exception {
        BookingOrderResource bookingOrderResource = new BookingOrderResource(bookingOrderServiceMock);
        when(bookingOrderServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        MockMvc restBookingOrderMockMvc = MockMvcBuilders.standaloneSetup(bookingOrderResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restBookingOrderMockMvc.perform(get("/api/booking-orders?eagerload=true"))
        .andExpect(status().isOk());

        verify(bookingOrderServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({"unchecked"})
    public void getAllBookingOrdersWithEagerRelationshipsIsNotEnabled() throws Exception {
        BookingOrderResource bookingOrderResource = new BookingOrderResource(bookingOrderServiceMock);
            when(bookingOrderServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));
            MockMvc restBookingOrderMockMvc = MockMvcBuilders.standaloneSetup(bookingOrderResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restBookingOrderMockMvc.perform(get("/api/booking-orders?eagerload=true"))
        .andExpect(status().isOk());

            verify(bookingOrderServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    @Transactional
    public void getBookingOrder() throws Exception {
        // Initialize the database
        bookingOrderRepository.saveAndFlush(bookingOrder);

        // Get the bookingOrder
        restBookingOrderMockMvc.perform(get("/api/booking-orders/{id}", bookingOrder.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(bookingOrder.getId().intValue()))
            .andExpect(jsonPath("$.orderDate").value(DEFAULT_ORDER_DATE.toString()))
            .andExpect(jsonPath("$.totalBill").value(DEFAULT_TOTAL_BILL.doubleValue()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingBookingOrder() throws Exception {
        // Get the bookingOrder
        restBookingOrderMockMvc.perform(get("/api/booking-orders/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBookingOrder() throws Exception {
        // Initialize the database
        bookingOrderService.save(bookingOrder);

        int databaseSizeBeforeUpdate = bookingOrderRepository.findAll().size();

        // Update the bookingOrder
        BookingOrder updatedBookingOrder = bookingOrderRepository.findById(bookingOrder.getId()).get();
        // Disconnect from session so that the updates on updatedBookingOrder are not directly saved in db
        em.detach(updatedBookingOrder);
        updatedBookingOrder
            .orderDate(UPDATED_ORDER_DATE)
            .totalBill(UPDATED_TOTAL_BILL)
            .status(UPDATED_STATUS);

        restBookingOrderMockMvc.perform(put("/api/booking-orders")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedBookingOrder)))
            .andExpect(status().isOk());

        // Validate the BookingOrder in the database
        List<BookingOrder> bookingOrderList = bookingOrderRepository.findAll();
        assertThat(bookingOrderList).hasSize(databaseSizeBeforeUpdate);
        BookingOrder testBookingOrder = bookingOrderList.get(bookingOrderList.size() - 1);
        assertThat(testBookingOrder.getOrderDate()).isEqualTo(UPDATED_ORDER_DATE);
        assertThat(testBookingOrder.getTotalBill()).isEqualTo(UPDATED_TOTAL_BILL);
        assertThat(testBookingOrder.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void updateNonExistingBookingOrder() throws Exception {
        int databaseSizeBeforeUpdate = bookingOrderRepository.findAll().size();

        // Create the BookingOrder

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBookingOrderMockMvc.perform(put("/api/booking-orders")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bookingOrder)))
            .andExpect(status().isBadRequest());

        // Validate the BookingOrder in the database
        List<BookingOrder> bookingOrderList = bookingOrderRepository.findAll();
        assertThat(bookingOrderList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteBookingOrder() throws Exception {
        // Initialize the database
        bookingOrderService.save(bookingOrder);

        int databaseSizeBeforeDelete = bookingOrderRepository.findAll().size();

        // Delete the bookingOrder
        restBookingOrderMockMvc.perform(delete("/api/booking-orders/{id}", bookingOrder.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<BookingOrder> bookingOrderList = bookingOrderRepository.findAll();
        assertThat(bookingOrderList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(BookingOrder.class);
        BookingOrder bookingOrder1 = new BookingOrder();
        bookingOrder1.setId(1L);
        BookingOrder bookingOrder2 = new BookingOrder();
        bookingOrder2.setId(bookingOrder1.getId());
        assertThat(bookingOrder1).isEqualTo(bookingOrder2);
        bookingOrder2.setId(2L);
        assertThat(bookingOrder1).isNotEqualTo(bookingOrder2);
        bookingOrder1.setId(null);
        assertThat(bookingOrder1).isNotEqualTo(bookingOrder2);
    }
}
