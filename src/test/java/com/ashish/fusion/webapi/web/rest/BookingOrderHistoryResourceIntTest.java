package com.ashish.fusion.webapi.web.rest;

import com.ashish.fusion.webapi.WebapiApp;

import com.ashish.fusion.webapi.domain.BookingOrderHistory;
import com.ashish.fusion.webapi.repository.BookingOrderHistoryRepository;
import com.ashish.fusion.webapi.service.BookingOrderHistoryService;
import com.ashish.fusion.webapi.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;


import static com.ashish.fusion.webapi.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the BookingOrderHistoryResource REST controller.
 *
 * @see BookingOrderHistoryResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WebapiApp.class)
public class BookingOrderHistoryResourceIntTest {

    private static final LocalDate DEFAULT_ORDER_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_ORDER_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final Float DEFAULT_TOTAL_BILL = 1F;
    private static final Float UPDATED_TOTAL_BILL = 2F;

    private static final String DEFAULT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_STATUS = "BBBBBBBBBB";

    @Autowired
    private BookingOrderHistoryRepository bookingOrderHistoryRepository;

    @Mock
    private BookingOrderHistoryRepository bookingOrderHistoryRepositoryMock;

    @Mock
    private BookingOrderHistoryService bookingOrderHistoryServiceMock;

    @Autowired
    private BookingOrderHistoryService bookingOrderHistoryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restBookingOrderHistoryMockMvc;

    private BookingOrderHistory bookingOrderHistory;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final BookingOrderHistoryResource bookingOrderHistoryResource = new BookingOrderHistoryResource(bookingOrderHistoryService);
        this.restBookingOrderHistoryMockMvc = MockMvcBuilders.standaloneSetup(bookingOrderHistoryResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BookingOrderHistory createEntity(EntityManager em) {
        BookingOrderHistory bookingOrderHistory = new BookingOrderHistory()
            .orderDate(DEFAULT_ORDER_DATE)
            .totalBill(DEFAULT_TOTAL_BILL)
            .status(DEFAULT_STATUS);
        return bookingOrderHistory;
    }

    @Before
    public void initTest() {
        bookingOrderHistory = createEntity(em);
    }

    @Test
    @Transactional
    public void createBookingOrderHistory() throws Exception {
        int databaseSizeBeforeCreate = bookingOrderHistoryRepository.findAll().size();

        // Create the BookingOrderHistory
        restBookingOrderHistoryMockMvc.perform(post("/api/booking-order-histories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bookingOrderHistory)))
            .andExpect(status().isCreated());

        // Validate the BookingOrderHistory in the database
        List<BookingOrderHistory> bookingOrderHistoryList = bookingOrderHistoryRepository.findAll();
        assertThat(bookingOrderHistoryList).hasSize(databaseSizeBeforeCreate + 1);
        BookingOrderHistory testBookingOrderHistory = bookingOrderHistoryList.get(bookingOrderHistoryList.size() - 1);
        assertThat(testBookingOrderHistory.getOrderDate()).isEqualTo(DEFAULT_ORDER_DATE);
        assertThat(testBookingOrderHistory.getTotalBill()).isEqualTo(DEFAULT_TOTAL_BILL);
        assertThat(testBookingOrderHistory.getStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    public void createBookingOrderHistoryWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = bookingOrderHistoryRepository.findAll().size();

        // Create the BookingOrderHistory with an existing ID
        bookingOrderHistory.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restBookingOrderHistoryMockMvc.perform(post("/api/booking-order-histories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bookingOrderHistory)))
            .andExpect(status().isBadRequest());

        // Validate the BookingOrderHistory in the database
        List<BookingOrderHistory> bookingOrderHistoryList = bookingOrderHistoryRepository.findAll();
        assertThat(bookingOrderHistoryList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllBookingOrderHistories() throws Exception {
        // Initialize the database
        bookingOrderHistoryRepository.saveAndFlush(bookingOrderHistory);

        // Get all the bookingOrderHistoryList
        restBookingOrderHistoryMockMvc.perform(get("/api/booking-order-histories?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(bookingOrderHistory.getId().intValue())))
            .andExpect(jsonPath("$.[*].orderDate").value(hasItem(DEFAULT_ORDER_DATE.toString())))
            .andExpect(jsonPath("$.[*].totalBill").value(hasItem(DEFAULT_TOTAL_BILL.doubleValue())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())));
    }
    
    @SuppressWarnings({"unchecked"})
    public void getAllBookingOrderHistoriesWithEagerRelationshipsIsEnabled() throws Exception {
        BookingOrderHistoryResource bookingOrderHistoryResource = new BookingOrderHistoryResource(bookingOrderHistoryServiceMock);
        when(bookingOrderHistoryServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        MockMvc restBookingOrderHistoryMockMvc = MockMvcBuilders.standaloneSetup(bookingOrderHistoryResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restBookingOrderHistoryMockMvc.perform(get("/api/booking-order-histories?eagerload=true"))
        .andExpect(status().isOk());

        verify(bookingOrderHistoryServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({"unchecked"})
    public void getAllBookingOrderHistoriesWithEagerRelationshipsIsNotEnabled() throws Exception {
        BookingOrderHistoryResource bookingOrderHistoryResource = new BookingOrderHistoryResource(bookingOrderHistoryServiceMock);
            when(bookingOrderHistoryServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));
            MockMvc restBookingOrderHistoryMockMvc = MockMvcBuilders.standaloneSetup(bookingOrderHistoryResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restBookingOrderHistoryMockMvc.perform(get("/api/booking-order-histories?eagerload=true"))
        .andExpect(status().isOk());

            verify(bookingOrderHistoryServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    @Transactional
    public void getBookingOrderHistory() throws Exception {
        // Initialize the database
        bookingOrderHistoryRepository.saveAndFlush(bookingOrderHistory);

        // Get the bookingOrderHistory
        restBookingOrderHistoryMockMvc.perform(get("/api/booking-order-histories/{id}", bookingOrderHistory.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(bookingOrderHistory.getId().intValue()))
            .andExpect(jsonPath("$.orderDate").value(DEFAULT_ORDER_DATE.toString()))
            .andExpect(jsonPath("$.totalBill").value(DEFAULT_TOTAL_BILL.doubleValue()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingBookingOrderHistory() throws Exception {
        // Get the bookingOrderHistory
        restBookingOrderHistoryMockMvc.perform(get("/api/booking-order-histories/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBookingOrderHistory() throws Exception {
        // Initialize the database
        bookingOrderHistoryService.save(bookingOrderHistory);

        int databaseSizeBeforeUpdate = bookingOrderHistoryRepository.findAll().size();

        // Update the bookingOrderHistory
        BookingOrderHistory updatedBookingOrderHistory = bookingOrderHistoryRepository.findById(bookingOrderHistory.getId()).get();
        // Disconnect from session so that the updates on updatedBookingOrderHistory are not directly saved in db
        em.detach(updatedBookingOrderHistory);
        updatedBookingOrderHistory
            .orderDate(UPDATED_ORDER_DATE)
            .totalBill(UPDATED_TOTAL_BILL)
            .status(UPDATED_STATUS);

        restBookingOrderHistoryMockMvc.perform(put("/api/booking-order-histories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedBookingOrderHistory)))
            .andExpect(status().isOk());

        // Validate the BookingOrderHistory in the database
        List<BookingOrderHistory> bookingOrderHistoryList = bookingOrderHistoryRepository.findAll();
        assertThat(bookingOrderHistoryList).hasSize(databaseSizeBeforeUpdate);
        BookingOrderHistory testBookingOrderHistory = bookingOrderHistoryList.get(bookingOrderHistoryList.size() - 1);
        assertThat(testBookingOrderHistory.getOrderDate()).isEqualTo(UPDATED_ORDER_DATE);
        assertThat(testBookingOrderHistory.getTotalBill()).isEqualTo(UPDATED_TOTAL_BILL);
        assertThat(testBookingOrderHistory.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void updateNonExistingBookingOrderHistory() throws Exception {
        int databaseSizeBeforeUpdate = bookingOrderHistoryRepository.findAll().size();

        // Create the BookingOrderHistory

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBookingOrderHistoryMockMvc.perform(put("/api/booking-order-histories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bookingOrderHistory)))
            .andExpect(status().isBadRequest());

        // Validate the BookingOrderHistory in the database
        List<BookingOrderHistory> bookingOrderHistoryList = bookingOrderHistoryRepository.findAll();
        assertThat(bookingOrderHistoryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteBookingOrderHistory() throws Exception {
        // Initialize the database
        bookingOrderHistoryService.save(bookingOrderHistory);

        int databaseSizeBeforeDelete = bookingOrderHistoryRepository.findAll().size();

        // Delete the bookingOrderHistory
        restBookingOrderHistoryMockMvc.perform(delete("/api/booking-order-histories/{id}", bookingOrderHistory.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<BookingOrderHistory> bookingOrderHistoryList = bookingOrderHistoryRepository.findAll();
        assertThat(bookingOrderHistoryList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(BookingOrderHistory.class);
        BookingOrderHistory bookingOrderHistory1 = new BookingOrderHistory();
        bookingOrderHistory1.setId(1L);
        BookingOrderHistory bookingOrderHistory2 = new BookingOrderHistory();
        bookingOrderHistory2.setId(bookingOrderHistory1.getId());
        assertThat(bookingOrderHistory1).isEqualTo(bookingOrderHistory2);
        bookingOrderHistory2.setId(2L);
        assertThat(bookingOrderHistory1).isNotEqualTo(bookingOrderHistory2);
        bookingOrderHistory1.setId(null);
        assertThat(bookingOrderHistory1).isNotEqualTo(bookingOrderHistory2);
    }
}
