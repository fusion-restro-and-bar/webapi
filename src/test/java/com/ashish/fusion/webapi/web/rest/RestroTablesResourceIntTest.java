package com.ashish.fusion.webapi.web.rest;

import com.ashish.fusion.webapi.WebapiApp;

import com.ashish.fusion.webapi.domain.RestroTables;
import com.ashish.fusion.webapi.repository.RestroTablesRepository;
import com.ashish.fusion.webapi.service.RestroTablesService;
import com.ashish.fusion.webapi.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;


import static com.ashish.fusion.webapi.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the RestroTablesResource REST controller.
 *
 * @see RestroTablesResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WebapiApp.class)
public class RestroTablesResourceIntTest {

    private static final String DEFAULT_TABLE_NAME = "AAAAAAAAAA";
    private static final String UPDATED_TABLE_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_STATUS = "BBBBBBBBBB";

    private static final Boolean DEFAULT_IS_DELETED = false;
    private static final Boolean UPDATED_IS_DELETED = true;

    @Autowired
    private RestroTablesRepository restroTablesRepository;

    @Autowired
    private RestroTablesService restroTablesService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restRestroTablesMockMvc;

    private RestroTables restroTables;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RestroTablesResource restroTablesResource = new RestroTablesResource(restroTablesService);
        this.restRestroTablesMockMvc = MockMvcBuilders.standaloneSetup(restroTablesResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RestroTables createEntity(EntityManager em) {
        RestroTables restroTables = new RestroTables()
            .tableName(DEFAULT_TABLE_NAME)
            .status(DEFAULT_STATUS)
            .isDeleted(DEFAULT_IS_DELETED);
        return restroTables;
    }

    @Before
    public void initTest() {
        restroTables = createEntity(em);
    }

    @Test
    @Transactional
    public void createRestroTables() throws Exception {
        int databaseSizeBeforeCreate = restroTablesRepository.findAll().size();

        // Create the RestroTables
        restRestroTablesMockMvc.perform(post("/api/restro-tables")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(restroTables)))
            .andExpect(status().isCreated());

        // Validate the RestroTables in the database
        List<RestroTables> restroTablesList = restroTablesRepository.findAll();
        assertThat(restroTablesList).hasSize(databaseSizeBeforeCreate + 1);
        RestroTables testRestroTables = restroTablesList.get(restroTablesList.size() - 1);
        assertThat(testRestroTables.getTableName()).isEqualTo(DEFAULT_TABLE_NAME);
        assertThat(testRestroTables.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testRestroTables.isIsDeleted()).isEqualTo(DEFAULT_IS_DELETED);
    }

    @Test
    @Transactional
    public void createRestroTablesWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = restroTablesRepository.findAll().size();

        // Create the RestroTables with an existing ID
        restroTables.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRestroTablesMockMvc.perform(post("/api/restro-tables")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(restroTables)))
            .andExpect(status().isBadRequest());

        // Validate the RestroTables in the database
        List<RestroTables> restroTablesList = restroTablesRepository.findAll();
        assertThat(restroTablesList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllRestroTables() throws Exception {
        // Initialize the database
        restroTablesRepository.saveAndFlush(restroTables);

        // Get all the restroTablesList
        restRestroTablesMockMvc.perform(get("/api/restro-tables?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(restroTables.getId().intValue())))
            .andExpect(jsonPath("$.[*].tableName").value(hasItem(DEFAULT_TABLE_NAME.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].isDeleted").value(hasItem(DEFAULT_IS_DELETED.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getRestroTables() throws Exception {
        // Initialize the database
        restroTablesRepository.saveAndFlush(restroTables);

        // Get the restroTables
        restRestroTablesMockMvc.perform(get("/api/restro-tables/{id}", restroTables.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(restroTables.getId().intValue()))
            .andExpect(jsonPath("$.tableName").value(DEFAULT_TABLE_NAME.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.isDeleted").value(DEFAULT_IS_DELETED.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingRestroTables() throws Exception {
        // Get the restroTables
        restRestroTablesMockMvc.perform(get("/api/restro-tables/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRestroTables() throws Exception {
        // Initialize the database
        restroTablesService.save(restroTables);

        int databaseSizeBeforeUpdate = restroTablesRepository.findAll().size();

        // Update the restroTables
        RestroTables updatedRestroTables = restroTablesRepository.findById(restroTables.getId()).get();
        // Disconnect from session so that the updates on updatedRestroTables are not directly saved in db
        em.detach(updatedRestroTables);
        updatedRestroTables
            .tableName(UPDATED_TABLE_NAME)
            .status(UPDATED_STATUS)
            .isDeleted(UPDATED_IS_DELETED);

        restRestroTablesMockMvc.perform(put("/api/restro-tables")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedRestroTables)))
            .andExpect(status().isOk());

        // Validate the RestroTables in the database
        List<RestroTables> restroTablesList = restroTablesRepository.findAll();
        assertThat(restroTablesList).hasSize(databaseSizeBeforeUpdate);
        RestroTables testRestroTables = restroTablesList.get(restroTablesList.size() - 1);
        assertThat(testRestroTables.getTableName()).isEqualTo(UPDATED_TABLE_NAME);
        assertThat(testRestroTables.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testRestroTables.isIsDeleted()).isEqualTo(UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void updateNonExistingRestroTables() throws Exception {
        int databaseSizeBeforeUpdate = restroTablesRepository.findAll().size();

        // Create the RestroTables

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRestroTablesMockMvc.perform(put("/api/restro-tables")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(restroTables)))
            .andExpect(status().isBadRequest());

        // Validate the RestroTables in the database
        List<RestroTables> restroTablesList = restroTablesRepository.findAll();
        assertThat(restroTablesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteRestroTables() throws Exception {
        // Initialize the database
        restroTablesService.save(restroTables);

        int databaseSizeBeforeDelete = restroTablesRepository.findAll().size();

        // Delete the restroTables
        restRestroTablesMockMvc.perform(delete("/api/restro-tables/{id}", restroTables.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<RestroTables> restroTablesList = restroTablesRepository.findAll();
        assertThat(restroTablesList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RestroTables.class);
        RestroTables restroTables1 = new RestroTables();
        restroTables1.setId(1L);
        RestroTables restroTables2 = new RestroTables();
        restroTables2.setId(restroTables1.getId());
        assertThat(restroTables1).isEqualTo(restroTables2);
        restroTables2.setId(2L);
        assertThat(restroTables1).isNotEqualTo(restroTables2);
        restroTables1.setId(null);
        assertThat(restroTables1).isNotEqualTo(restroTables2);
    }
}
