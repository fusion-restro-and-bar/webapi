package com.ashish.fusion.webapi.web.rest;

import com.ashish.fusion.webapi.WebapiApp;

import com.ashish.fusion.webapi.domain.MenuCategory;
import com.ashish.fusion.webapi.repository.MenuCategoryRepository;
import com.ashish.fusion.webapi.service.MenuCategoryService;
import com.ashish.fusion.webapi.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;


import static com.ashish.fusion.webapi.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the MenuCategoryResource REST controller.
 *
 * @see MenuCategoryResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WebapiApp.class)
public class MenuCategoryResourceIntTest {

    private static final String DEFAULT_CATEGORY_NAME = "AAAAAAAAAA";
    private static final String UPDATED_CATEGORY_NAME = "BBBBBBBBBB";

    @Autowired
    private MenuCategoryRepository menuCategoryRepository;

    @Autowired
    private MenuCategoryService menuCategoryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restMenuCategoryMockMvc;

    private MenuCategory menuCategory;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final MenuCategoryResource menuCategoryResource = new MenuCategoryResource(menuCategoryService);
        this.restMenuCategoryMockMvc = MockMvcBuilders.standaloneSetup(menuCategoryResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MenuCategory createEntity(EntityManager em) {
        MenuCategory menuCategory = new MenuCategory()
            .categoryName(DEFAULT_CATEGORY_NAME);
        return menuCategory;
    }

    @Before
    public void initTest() {
        menuCategory = createEntity(em);
    }

    @Test
    @Transactional
    public void createMenuCategory() throws Exception {
        int databaseSizeBeforeCreate = menuCategoryRepository.findAll().size();

        // Create the MenuCategory
        restMenuCategoryMockMvc.perform(post("/api/menu-categories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(menuCategory)))
            .andExpect(status().isCreated());

        // Validate the MenuCategory in the database
        List<MenuCategory> menuCategoryList = menuCategoryRepository.findAll();
        assertThat(menuCategoryList).hasSize(databaseSizeBeforeCreate + 1);
        MenuCategory testMenuCategory = menuCategoryList.get(menuCategoryList.size() - 1);
        assertThat(testMenuCategory.getCategoryName()).isEqualTo(DEFAULT_CATEGORY_NAME);
    }

    @Test
    @Transactional
    public void createMenuCategoryWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = menuCategoryRepository.findAll().size();

        // Create the MenuCategory with an existing ID
        menuCategory.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restMenuCategoryMockMvc.perform(post("/api/menu-categories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(menuCategory)))
            .andExpect(status().isBadRequest());

        // Validate the MenuCategory in the database
        List<MenuCategory> menuCategoryList = menuCategoryRepository.findAll();
        assertThat(menuCategoryList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllMenuCategories() throws Exception {
        // Initialize the database
        menuCategoryRepository.saveAndFlush(menuCategory);

        // Get all the menuCategoryList
        restMenuCategoryMockMvc.perform(get("/api/menu-categories?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(menuCategory.getId().intValue())))
            .andExpect(jsonPath("$.[*].categoryName").value(hasItem(DEFAULT_CATEGORY_NAME.toString())));
    }
    
    @Test
    @Transactional
    public void getMenuCategory() throws Exception {
        // Initialize the database
        menuCategoryRepository.saveAndFlush(menuCategory);

        // Get the menuCategory
        restMenuCategoryMockMvc.perform(get("/api/menu-categories/{id}", menuCategory.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(menuCategory.getId().intValue()))
            .andExpect(jsonPath("$.categoryName").value(DEFAULT_CATEGORY_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingMenuCategory() throws Exception {
        // Get the menuCategory
        restMenuCategoryMockMvc.perform(get("/api/menu-categories/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMenuCategory() throws Exception {
        // Initialize the database
        menuCategoryService.save(menuCategory);

        int databaseSizeBeforeUpdate = menuCategoryRepository.findAll().size();

        // Update the menuCategory
        MenuCategory updatedMenuCategory = menuCategoryRepository.findById(menuCategory.getId()).get();
        // Disconnect from session so that the updates on updatedMenuCategory are not directly saved in db
        em.detach(updatedMenuCategory);
        updatedMenuCategory
            .categoryName(UPDATED_CATEGORY_NAME);

        restMenuCategoryMockMvc.perform(put("/api/menu-categories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedMenuCategory)))
            .andExpect(status().isOk());

        // Validate the MenuCategory in the database
        List<MenuCategory> menuCategoryList = menuCategoryRepository.findAll();
        assertThat(menuCategoryList).hasSize(databaseSizeBeforeUpdate);
        MenuCategory testMenuCategory = menuCategoryList.get(menuCategoryList.size() - 1);
        assertThat(testMenuCategory.getCategoryName()).isEqualTo(UPDATED_CATEGORY_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingMenuCategory() throws Exception {
        int databaseSizeBeforeUpdate = menuCategoryRepository.findAll().size();

        // Create the MenuCategory

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMenuCategoryMockMvc.perform(put("/api/menu-categories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(menuCategory)))
            .andExpect(status().isBadRequest());

        // Validate the MenuCategory in the database
        List<MenuCategory> menuCategoryList = menuCategoryRepository.findAll();
        assertThat(menuCategoryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteMenuCategory() throws Exception {
        // Initialize the database
        menuCategoryService.save(menuCategory);

        int databaseSizeBeforeDelete = menuCategoryRepository.findAll().size();

        // Delete the menuCategory
        restMenuCategoryMockMvc.perform(delete("/api/menu-categories/{id}", menuCategory.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<MenuCategory> menuCategoryList = menuCategoryRepository.findAll();
        assertThat(menuCategoryList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(MenuCategory.class);
        MenuCategory menuCategory1 = new MenuCategory();
        menuCategory1.setId(1L);
        MenuCategory menuCategory2 = new MenuCategory();
        menuCategory2.setId(menuCategory1.getId());
        assertThat(menuCategory1).isEqualTo(menuCategory2);
        menuCategory2.setId(2L);
        assertThat(menuCategory1).isNotEqualTo(menuCategory2);
        menuCategory1.setId(null);
        assertThat(menuCategory1).isNotEqualTo(menuCategory2);
    }
}
